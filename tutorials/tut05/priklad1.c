#include <stdio.h>

int main()
{
    int a;      // aktualne zpracovavane cislo
    int b;      // pamet pro jedno cislo z minule iterace
    int c = 0;  // globalni pocitadlo opakovani, bez ohledu na hodnotu opakovaneho cisla
    int d = 0;  // pocitadlo opakovani pro dane cislo (ale ne histogram, pouze "po sobe jdouci opakovani")
    int i = 0;  // pocitadlo iteraci

    while(scanf("%i", &a) != EOF)  // 1 - podarila se konverze a ulozeni na &a, 0 - nepodarilo se, -1/EOF - konec dat
    {
        if (i++ == 0)
        {
            printf("%i ", a);
        }
        else
        {
            // printf(" (%i) %i", b, a);
            printf("%i ", a);
            if (b == a)
            {
                printf("(%d/%i) ", ++d, ++c);
            }
            else
            {
                d = 0;
            }
            
        }
        b = a;
    }
    printf("\n");

    return 0;
}