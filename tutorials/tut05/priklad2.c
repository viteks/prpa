#include <stdio.h>

int main()
{
/*
    *****
    *****
    *****
    *****
*/
    // vnejsi cyklus -- opakuje kresleni radku
    for (int j = 0; j < 5; j++)
    {
        // vnitrni cyklus -- kresli radku
        for (int i = 0; i < 5; i++)
        {
            // ternarni operator: (log) ? true : false;
            printf((i<j)?" ":"*");
            /*
            if (i < j)
            {
                printf(" ");
            }
            else
            {
                printf("*");
            }
            */
        }
        printf("\n");
    }
}