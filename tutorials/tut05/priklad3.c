#include <stdio.h>

int main()
{
    for (int i = 0; i < 10; i++)
    {
        for (int j = 0; j < 10; j++)
        {
            printf("%2i ", j + 10*i);
            // printf("%2i ", j); - manipulace s mezi vnitrniho cyklu: for (int j = 0+i*10; j < 10+i*10; j++)
            // printf("%i%i ", i, j);
        }
        printf("\n");
    }

    for (int i = 0; i < 100; i++)
    {   
        // if (i%10 == 0) printf("\n");
        printf("%2i ", i);
        if (i%10 == 9) printf("\n");
    }

    // diagonala
    for (int i = 0; i < 10; i++)
    {
        for (int j = 0; j < 10; j++)
        {
            if (i == j)
            {
                printf("%2i ", j + 10*i);
                break;
            }
            else
            {
                printf("   ");
            }
            
        }
        printf("\n");
    }

    // vedlejsi diagonala
    for (int i = 0; i < 100; i++)
    {   
        if (i%9 == 0 && i > 0 && i < 99)
        {
            printf("%2i ", i);
        }
        else
        {
            printf("   ");
        }
        
        if (i%10 == 9) printf("\n");
    }

    // obe diagonaly
    for (int i = 0; i < 100; i++)
    {
        // hledam cislo delitene 3
        // i%3 -> 0(false) 1(true) 2(true)
        // potrebuji, aby podminka byla vyhodnocena jako true, 
        // prave, kdyz hodnota vyrazu je false
        // 1. soucast vyrazu: i%3 == 0
        // 2. negace hodnoty vyrazu: !(i%3)
        if (i%9 == 0 || !(i%11))
        {
            printf("%2i ", i);
        }
        else
        {
            printf("   ");
        }
        
        if (i%10 == 9) printf("\n");
    }
}