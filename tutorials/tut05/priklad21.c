#include <stdio.h>

void kresli (char typ)
{
    printf("(%c)\n", typ);
    // vnejsi cyklus -- opakuje kresleni radku
    for (int j = 0; j < 5; j++)
    {
        // vnitrni cyklus -- kresli radku
        for (int i = 0; i < 5; i++)
        {
            switch (typ)
            {
                case 'a':
                    printf("*");
                    break;
                case 'b':
                    printf((i-1<j)?"*":" ");
                    break;
                case 'c':
                    printf((i+1<(5-j))?" ":"*");
                    break;
                case 'd':
                    printf((i<(5-j))?"*":" ");
                    break;
                case 'e': 
                    printf((i<j)?" ":"*");
                    break;
            }
        }
        printf("\n");
    }
    printf("\n");
}

int main()
{
    kresli('a');
    kresli('b');
    kresli('c');
    kresli('d');
    kresli('e');

    return 0;
}