#include <stdio.h>

// globalni promenna
int b = 2;

int main()
{
    // lokalni promenna, plati v bloku ohranicenem {}
    int a = 10, c, d = 100;
    int e;
    float f = 3.148;
    int g = 100;

    printf("a = %05i\nb = %f\n", a, b);

    printf("0123456789\n%10.3f\n%i\n", f, (int)f);

    printf("%c\n", g);

    return 0;
}
