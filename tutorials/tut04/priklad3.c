#include <stdio.h>
#include <math.h>

/*
 * M_PI is not defined in standard C. Provide your own definition 
 * if you want to be standard-compliant. C compilers cannot introduce 
 * such constants without breaking legal C programs (the name is not 
 * reserved, and could be used as an identifier), and as such, they 
 * are only defined as an extension.
 * GCC 4.9 when used with -std=c99 doesn't define M_PI, but does when used with -std=gnu99
 */

#ifndef M_PI
#define M_PI (3.14159265358979323846)
#endif

double pi (int n)
{
    int j = -1;
    double jm;
    double suma = 0.; // konstanta typu double

    for (int i = 0; i < n; i++)
    {
        j*=-1; // citatel clenu L. rady
        jm = 2*i+1;
        suma += j/jm;
    }
    return 4*suma;
}

int main()
{
    for (int n = 1000; n <= 10000; n+=1000)
    {
        double lpi = pi(n);
        printf("pi = %.10f M_PI = %.10f delta = %.10f\n", lpi, M_PI, lpi-M_PI);
    }
    return 0;
}