#include <stdio.h>

char hex (int i)
{
    switch (i)
    {
        case 0: return '0'; break;
        case 1: return '1'; break;
        case 2: return '2'; break;
        case 3: return '3'; break;
        case 4: return '4'; break;
        case 5: return '5'; break;
        case 6: return '6'; break;
        case 7: return '7'; break;
        case 8: return '8'; break;
        case 9: return '9'; break;
        case 10: return 'A'; break;
        case 11: return 'B'; break;
        case 12: return 'C'; break;
        case 13: return 'D'; break;
        case 14: return 'E'; break;
        case 15: return 'F'; break;
    }
    return '0';
}

int main()
{
    int N = 10;
    int i;
    int s = 0;
    short j = 10;

    for (i = 0 ; i < N; ++i)
    {
        // v ramci tohoto bloku bych mel nejak manipulovat
        // s ridici promennou, pokud to neudelam v definici cyklu
        // printf("i = %i\n", i);
        s += i; // ~ s = s + i
    }

    printf("suma = %i\n", s);

    while (j > 0)
    {
        printf("%i ", --j);
    }

    printf("\n");

    int M = 168;
    int Z = 16;

    while (M > 0)
    {
        printf("%c", hex(M % Z));
        M /= Z;
    }

    printf("\n");

    return 0;
}