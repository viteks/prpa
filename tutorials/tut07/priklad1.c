#include <stdio.h>

int f(int i)
{
    return i+10;
}

int main()
{
    for (int i = 0; i < 5; i++)
    {
        printf("i = %i\n", f(i));
    }

    return 0;
}