#include <stdio.h>

/*
    a[3][4] ~ vystupuji prvky dataveho typu ()[4]

    | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 |
    ^ adresa pole   ^ druhy radek   ^ treti radek
*/

void matice_napln (int *x, int w, int h)
{
    int a = 0;
    for (int j = 0; j < h; j++)
    {
        for (int i = 0; i < w; i++)
        {
            x[i+j*w] = a++;
        }
    }
}

void matice_tisk (int *x, int w, int h)
{
    for (int j = 0; j < h; j++)
    {
        for (int i = 0; i < w; i++)
        {
            printf("%3i ", x[i+j*w]);
        }
        printf("\n");
    }
}

void vektor_napln (int *x, int velikost)
{
    for (int i = 0; i < velikost; i++)
        x[i] = velikost - i;
}

void vektor_tisk (int *x, int velikost)
{
    for (int i = 0; i < velikost; i++)
    {
        printf("%2i ", x[i]);
    }
    printf("\n");
}

int vektor_soucet (int *a, int *b, int *c, int va, int vb, int vc)
{
    if (va != vb || vc != va)
    {
        return 100;
    }
    
    for (int i = 0; i < vc; i++)
    {
        c[i] = a[i] + b[i];
    }

    return 0;
}

int main()
{
    int w = 10;
    int a[w], b[w], c[w];

    int x = 5, y = 5;
    int m[x*y];

    vektor_napln (a, 10);
    vektor_napln (b, 10);
    vektor_soucet (a, a, c, w, w, w);
    vektor_tisk (c, w);

    matice_napln (m, x, y);
    matice_tisk (m, x, y);
}