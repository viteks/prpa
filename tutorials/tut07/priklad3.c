#include <stdio.h>
// void vektor_soucet (int velikost_p, velikost_q, int p[velikost], int q[velikost])

void nacti_matici (int *x, int w, int h)
{
    int a = 0;
    for (int j = 0; j < h; j++)
    {
        for (int i = 0; i < w; i++)
        {
            x[i+j*w] = a++; 
        }
    }
}

void tisk_matice (int *x, int w, int h)
{
    for (int j = 0; j < h; j++)
    {
        for (int i = 0; i < w; i++)
        {
            printf("%3i ", x[i+j*w]); 
        }
        printf("\n");
    }
}

void nacti_vektor (int *x, int w)
{
    for (int i = 0; i < w; i++)
    {
        x[i] = i;
    }
}

void tiskni_vektor(int *x, int w)
{
    for (int i = 0; i < w; i++)
    {
        printf("%i ", x[i]);
    }
    printf("\n");
}

int vektor_soucet (int *vektor_1, int *vektor_2, int *vysledek, int velikost_1, int velikost_2, int velikost_3)
{
    if ((velikost_1 != velikost_2) || (velikost_3 != velikost_1))
    {
        return 100;
    }
    
    for (int i = 0; i < velikost_3; i++)
    {
        vysledek[i] = vektor_1[i] + vektor_2[i];
    }

    return 0;
} 

int main()
{
    int w = 10;
    // pole variabilni delky nelze inicizalizovat pri deklaraci
    int a[w], b[w], c[w];
    int d[25];

    nacti_vektor(a, w);
    nacti_vektor(b, w);
    
    tiskni_vektor(a, w);
    tiskni_vektor(b, w);

    vektor_soucet(a, b, c, w, w, w);
    tiskni_vektor(c, w);

    nacti_matici(d, 5, 5);
    tisk_matice (d, 5, 5);
}