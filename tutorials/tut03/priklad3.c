/*
 * Napište program, který provede výpočet progresivní daně. 
 * Budeme uvažovat progresivní zdanění ve výši 10% pro příjem 
 * do 10000, 20% pro příjem od 10000 do 20000 a 30% pro příjem 
 * nad 20000. Například, pokud máme hrubou mzdu 24000, bude se
 * prvních 10000 danit 10% (tj. daň z této části mzdy je 1000),
 * dalších 10000 se daní 20% (daň z této části je 2000) a zbývající 
 * 4000 se daní 30% (daň je 1200). Celkovou výši daně pak vypočítáme 
 * jako součet jednotlivých částečných daní (tj. celková daň činí 4200).
 */

#include <stdio.h>

int main()
{
    int mzda = 24000;
    double dan = 0.;

    if ((mzda - 10000) >= 0)
    {
        // zrejme mzda je > 10k
        dan = 1000.;
    }
    else
    {
        // mzda je v intervalu 0 - 10k
        dan = mzda*0.1;
        printf("dan: %.2f\n", dan);
        return 0; 
    }
    
    mzda = mzda - 10000; // mzda -= 10000

    if ((mzda - 10000) >= 0)
    {
        // mzda je v > 20k
        dan += 2000.;
    }
    else
    {
        // mzda v intervalu 10k - 20k
        dan += mzda*0.2;
        printf("dan: %.2f\n", dan);
        return 0;
    }

    mzda -= 10000;

    dan += mzda*0.3;
    printf("dan: %.2f\n", dan);

    return 0;
}