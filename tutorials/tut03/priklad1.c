/*
 * Napište program, který načte ze standardního vstupu 
 * tři nenulová celá čisla reprezentující strany trojúhelníku 
 * a vyhodnotí, zda lze trojúhelník sestrojit. 
 * 
 * Definice chybovych stavu
 * 100 - chyba pri nacitani vstupnich dat
 * 101 - nektera ze stran je zaporna
 * 102 - trojuhelnik nelze sestrojit 
 */

#include <stdio.h>

int main()
{
    // strany trojuhelniku
    int a, b, c;
    // navratova hodnota scanf
    int d;
    // nacteni trech celych cisel oddelenych bilym znakem
    d = scanf("%i%i%i", &a, &b, &c);
    // informace o navratove hodnote scanf
    printf("navratova hodnota scanf: %i\n", d);
    // hodnoty nactenych dat
    printf("a = %i\n", a);
    printf("b = %i\n", b);
    printf("b = %i\n", c);
    // test poctu nactenych promennych - navratove hodnoty scanf
    if (d == 3) // = je operator prirazeni, == je operator ekvivalence
    {
        printf("vstupy byly nacteny spravne\n");
    }
    else
    {
        printf("pri nacitani vstupu doslo k chybe\n");
        return 100;
    }

    // vytvareni slozitejsich logickych vyrazu
    // AND  &&
    // OR   || 

    // test, zda jsou promenne kladne
    if (a < 0 || b < 0 || c < 0)
    {
        printf("nektera ze stran je zaporna\n");
        return 101;
    }

    // test, zda je mozne trojuhelnik sestavit
    if ( (a+b > c) && (a+c > b) && (b+c > a) )
    {
        printf("trojuhelnik lze sestrojit\n");
    }
    else
    {
        printf("trojuhelnik sestrojit nelze\n");
        return 102;
    }

    return 0;
}