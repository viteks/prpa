/* Napište program, který načte jeden znak (datový typ char) 
 * a tento znak klasifikuje do některé z následujících množin:
 *  - velké písmeno
 *  - malé písmeno
 *  - číslo
 *  - jiný znak
 */

#include <stdio.h>

int main()
{
    char c;

    if (scanf("%c", &c) != 1)
    {
        printf("doslo k chybe pri zadavani\n");
        return 100;
    }
    
    if (c >= 'A' && c <= 'Z')
    {
        printf("vstup patri do mnoziny velkych pismen\n");
    }
    else if (c >= 'a' && c <= 'z')
    {
        printf("vstup patri do mnoziny malych pismen\n");
    }
    else if (c >= '0' && c <= '9')
    {
        printf("vstup patri do mnoziny cisel\n");
    }
    else
    {
        printf("vstup nepatri do zadne mnoziny\n");
    }
    
    return 0;
}