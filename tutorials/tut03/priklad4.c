/* 
 * Napište program, který realizuje kurzový kalkulátor (převod 
 * mezi měnami). V programu budou definovány kurzy CZK vzhledem 
 * k dvěma konvertibilním měnám, konverze bude obousměrná. 
 * Program strukturujte do funkcí a procedur.
 */

#include <stdio.h>

#define CZKEUR   27

int menu()
{
    int a;

    printf("(1) EUR -> CZK\n");
    printf("(2) CZK -> EUR\n > ");
    
    scanf("%i", &a);

    if (a > 2) 
    {
        printf("spatna volba\n");
        a = menu();
    }
    return a;
}

int main()
{
    int czk;    // ceska koruna
    int eur;    // euro
    double kurz = CZKEUR;
    int volba;

    volba = menu();

    if (volba == 1)
    {
        printf("zadej castku v EUR: ");
        scanf("%i", &eur);
        czk = kurz * eur;
        printf("castka v CZK: %i\n", czk);
    }
    else if (volba == 2)
    {
        printf("zadej castku v CZK: ");
        scanf("%i", &czk);
        eur = czk / kurz; // (1/kurz) * czk
        printf("castks v EUR: %i\n", eur);
    }

    return 0;
}