#include <stdio.h>

struct k
{
    float real;
    float imag;
};

typedef struct
{
    float real;
    float imag;
} komplex;

void tisk (struct k x)
{
    printf("%.1f%+.1fj\n", x.real, x.imag);
}

void tisk2 (komplex x)
{
    printf("%.1f%+.1fj\n", x.real, x.imag);
}

struct k soucet (struct k x, struct k y)
{
    struct k tmp;
    tmp.real = x.real + y.real;
    tmp.imag = x.imag + y.imag;
    return tmp;
}

/*
    void f(int * a)
    {
        *a = 10;
    }
*/

void soucet2 (komplex x, komplex y, komplex * z)
{
    (*z).real = x.real + y.real;
    z->imag = x.imag + y.imag;
}

int main()
{
    // komplexni cislo K
    // float K_real, K_imag;
    // komplexni cislo L
    // float L_real, L_imag;

    struct k a, b = {3.3, -4.4}, c = {.imag = 5.5, .real=-9.0};
    komplex d = {1.1, 2.2}, e = {8.2, 4.4}, f = {1.1, 4.4};

    a.real = 1.1;
    a.imag = 2.2;
    
    d = (komplex){2.2, 3.3};

    tisk(a);
    tisk(b);
    tisk(c);
    tisk(soucet(a, b));
    tisk2(d);
    soucet2 (e, f, &d);
    tisk2(d);
}