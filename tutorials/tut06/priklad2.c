#include <stdio.h>

typedef struct 
{
    int velikost;
    int *pole;
} arr;


/*
void tisk (int x, int a[x])
{

}
*/

void tisk (int * a, int velikost)
{
    for (int i = 0; i < velikost; i++)
    {
        // printf("%i ", *(a+i));  // vytiskni obsah promenne, ktera je na adrese a + i*sizeof(int) ~ a[i]
        printf("%i ", a[i]);
    }

    // int a[10]
    // int *p = a;
    // *(p+i) ~ a[i], ale zaroven je mozne pouzivat i p[i]
} 

void tisk2 (arr x)
{
    for (int i = 0; i < x.velikost; i++)
        printf("%i ", *(x.pole+i));
    printf("\n");
}

int main()
{
    // neinicializovane pole 10 prvku typu int
    int a[10];
    // inicializace pri deklaraci
    int b[] = {1, 2, 3};

    // arr B = {3, b};

    // pole variabilni delky
    printf("zadej pocet cisel: ");
    int n, i = 0;
    scanf("%i", &n);
    int c[n];
    arr C = {n, c};

    while (i < n)
    {
        int a;
        scanf ("%i", &a);
        c[i++] = a;
    }

    // tisk(c, size(c)/size(int));
    tisk2(C);

    return 0;
}