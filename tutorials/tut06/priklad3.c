#include <stdio.h>

void tisk(int *, int);
void histogram (int *, int);

int main()
{
    int n = 1000;
    int pole[n];
    int *p = pole;

    for (int i = 0; i < n; i++)
    {
        // int a;
        // scanf("%i", &a);
        // pole[i] = a;
        scanf("%i", p++);
    }
    tisk(pole, n);

    histogram(pole, n);

    return 0;
}

void tisk (int * p, int velikost)
{
    for (int i = 0; i < velikost; i++)
    {
        printf("%3i ", p[i]);
        if (i%20 == 19) printf("\n");
    }
    printf("\n");
}

void histogram (int * p, int velikost)
{
    // int h0 = 0, h1 = 0, h2 = 0;  
    int h[100] = {0};

    for (int i = 0; i < velikost; i++)
    {
        h[p[i]]++;
    }

    tisk(h, 100);

    for (int j = 0; j < 100; j++)
    for (int i = 0; i < 99; i++)
    {
        if (h[i] > h[i+1])
        {
            int tmp = h[i];
            h[i] = h[i+1];
            h[i+1] = tmp;
        }
    }   
    tisk(h, 100);
}