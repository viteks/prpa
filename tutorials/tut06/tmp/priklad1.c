#include <stdio.h>

// popis struktury
typedef struct kompl 
{
    float real;
    float imag;
} komplex;

// funkce se strukturou jako navratovou hodnotou
komplex soucet (komplex a, komplex b)
{
    komplex tmp;
    tmp.real = a.real + b.real;
    tmp.imag = a.imag + b.imag;
    return tmp;
}

/*
void f (int *a)
{
    *a = 10;
}
*/

// funkce, kde je predavan ukazatel na strukturu
void soucet2 (komplex a, komplex b, komplex * c)
{
    // (*c).real =
    c->real = a.real + b.real;
    c->imag = a.imag + b.imag;
}

void tisk (struct kompl x)
{
    printf("%.2f %+.2fj\n", x.real, x.imag);
}

void tisk2 (komplex x)
{
    printf("%.2f %+.2fj\n", x.real, x.imag);
}

int main()
{
    // komplexni cislo
    struct kompl a;
    komplex b; 
    komplex c = {3.3, 2.2};
    komplex d = {.imag = 5.5, .real = 6.6};

    a.real = 10.0;
    a.imag = 30.3;

    b.real = 10.0;
    b.imag = 30.3;

    // b = {4.4, 4.4} -> nelze
    b = (komplex){4.4, 4.4};

    tisk (a);
    tisk2 (b);
    tisk2 (soucet(a, b));

    tisk2 (d);
    soucet2 (a, b, &d);
    tisk2 (d);

    return 0;
}