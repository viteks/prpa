#include <stdio.h>

int main()
{
    int n, i = 0, a;

    printf ("zadej pocet cisel k nacteni: ");
    scanf ("%i", &n);
    // pole variablni delky - C99
    char pole[n];

    while (i < n)
    {
        scanf("%i", &a);
        pole[i++] = a;
    }

    for (i = 0; i < n; i++)
    {
        printf("%i ", pole[i]);
    }
    printf("\n");

    return 0;
}