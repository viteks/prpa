#include <stdio.h>

typedef struct 
{
    int velikost;
    int * pole;
} arr;

void tisk (char * pole, int n)
{
    for (int i = 0; i < n; i++)
    {
        printf("%3i ", pole[i]);
        if (i%20 == 19)
            printf("\n");

    }
    printf("\n");
}

void tisk_arr (arr x)
{
    for (int i = 0; i < x.velikost; i++)
    {
        printf("%i ", *(x.pole+i));
    }
    printf("\n\n");
}

void histogram (char * pole, int n)
{
    int h[100] = {0};
    arr H = {100, h};

    for (int i = 0; i < n; i++)
    {
        h[(int)pole[i]]++;
    }
/*
    for (int i = 0; i < 100; i++)
    {
        printf("%i ", h[i]);
    }
    printf("\n");
*/
    tisk_arr (H);
    // bubble sort -> porovnava dva prvky jdouci po sobe
    // nejmensi po nejvetsi
    for (int j = 0; j < 100; j++)
    {
        // printf("%i pruchod\n", j);
        int zmena = 0;
        for (int i = 0; i < 99; i++)
        {
            if (h[i] > h[i+1])
            {
                int tmp;
                tmp = h[i];
                h[i] = h[i+1];
                h[i+1] = tmp;
                zmena++;
            }
        }
        if (zmena == 0) break;
    }
/*
    for (int i = 0; i < 100; i++)
    {
        printf("%i ", h[i]);
    }
    printf("\n");
*/
    tisk_arr(H);
}

int main()
{
    int n = 1000, i = 0, a;
    char pole[n];

    for (i = 0; i < n; i++)
    {
        scanf("%i", &a);
        pole[i] = a;
    }

    // tisk (pole, n);
    histogram (pole, n);

    return 0;
}