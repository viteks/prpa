/* 
 * Napište program, který ze standardního vstupu načte dvě celá čísla 
 * (datový typ integer) reprezentující odvěsny pravoúhlého trojúhelníka 
 * a vypočítá délku přepony. Délku přepony vypište ve tvaru desetinného 
 * čísla s přesností na dvě desetinná místa. V programu vyuzijte funkci 
 * sqrt, která je definovaná v hlavičkovém souboru math.h.
 */

#include <stdio.h>
#include <math.h>

int main()
{
    int a, b;
    float c;

    scanf("%i%i", &a, &b);

    c = sqrt(a*a + b*b);

    printf("Odvesna je %.2f\n", c);

    return 0;
}