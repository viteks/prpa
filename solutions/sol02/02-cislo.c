/*
 * Napište program, který načte ze standardního vstupu reálné číslo, 
 * vypočítá celou a desetinnou část a vypíše ji na obrazovku. Desetinná
 * část bude vypsána s přesností na 4 desetinná čísla.
 */

#include <stdio.h>

int main()
{
    float a;

    scanf("%f", &a);

    printf("Cela cast: %i\n", (int)a);
    printf("Desetinna cast: %.4f\n", a - (int)a);

    return 0;
}