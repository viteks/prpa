/* 
 * Napište program, jehož vstupem bude reálné číslo a počet
 * desetinných míst, na které má být provedeno zkrácení.
 */

#include <stdio.h>
#include <math.h>

int main()
{
    float a, c;
    int b;

    scanf("%f%i", &a, &b);

    c = pow(10,b);

    a = (int)(a*c)/c;

    printf("Orez na %i desetinna mista: %.*f\n", b, b, a);

    return 0;
}