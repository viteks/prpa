#include <stdio.h>
#include <math.h>

double odmocnina (double cislo, double presnost)
{
	double horni_mez = cislo;
	double dolni_mez = 0;
	double stred = (horni_mez + dolni_mez) / 2;

	while (fabs(stred*stred - cislo) > presnost)
	{
        printf("%4.4f %4.4f %4.4f\n", dolni_mez, stred, horni_mez);
		if (stred*stred > cislo)
			horni_mez = stred;
		else
			dolni_mez = stred;
		stred = (horni_mez + dolni_mez) / 2;
    }
    return stred;
}

int main()
{
    float a, b;

    printf("zadej cislo: ");
    scanf("%f", &a);
    printf("zadej presnost: ");
    scanf("%f", &b);

    printf("odmocnina cisla %4.4f s presnosti %4.4f je %4.4f\n", a, b, odmocnina(a, b));

    return 0;
}