#include <stdio.h>

int main()
{
    int a, s = 0;

    printf("Zadej cele cislo: ");
    scanf("%i", &a);

    while (a > 0)
    {
        s += a % 10;
        a /= 10;
    }

    printf("Ciferny soucet cisla %i je %i\n", a, s);

    return 0;
}