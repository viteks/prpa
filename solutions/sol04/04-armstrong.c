#include <stdio.h>
#include <math.h>

int main() 
{
    int cislo, vstup, zbytek, n = 0;
    float vysledek = 0.0;

    printf("Napis cele cislo: ");
    scanf("%d", &vstup);

    // zjisteni poctu cifer cisla n
    for (cislo = vstup; cislo != 0; ++n) 
    {
        cislo /= 10;
    }

    for (cislo = vstup; cislo != 0; cislo /= 10) 
    {   
        zbytek = cislo % 10;
        // soucet n-tych mocnin jednotlivych cifer cisla
        vysledek += pow(zbytek, n);
    }

    // pokud je soucet n-tych mocnin cifer cisla roven cislu samotnemu, je to Armstrongovo cislo
    printf ("%d %s Armstrongovo cislo.\n", vstup,  ((int)vysledek == vstup) ? "je" : "neni");

    return 0;
}