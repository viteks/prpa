#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#define SEED 35791246

main(int argc, char *argv)
{
    int niter = 0; /* počet iterací algoritmu */
    double x, y;
    int count = 0; /* počet bodů v 1. kvadrantu jednotkové kružnice */
    double z;
    double pi;

    printf("Zadejte pocet iteraci pro vypocet PI: ");
    scanf("%d", &niter);

    /* initialize random numbers */
    srand(SEED);

    for (int i = 0; i < niter; i++)
    {
        x = (double)rand() / RAND_MAX;
        y = (double)rand() / RAND_MAX;
        z = x * x + y * y;
        if (z <= 1)
            count++;
    }
    pi = (double)count / niter * 4;
    printf("PI = %g / %i iteraci\n", pi, niter);

    return 0;
}