/*
 * Napište program, který načte ze standarního vstupu 
 * tři celá nenulová čísla, reprezentující strany obecného 
 * trojúhelníku. Vypočtěte obsah trojúhelníku, využijte 
 * Heronův vzorec.
 */

#include <stdio.h>
#include <math.h>

float heron (int a, int b, int c)
{
    float s, S;
    s = (a + b + b)/2.0;
    S = sqrt(s*(s-a)*(s-b)*(s-c));
    return S;
}

int main()
{
    unsigned int a, b, c;

    if (scanf("%i%i%i", &a, &b, &c) != 3)
    {
        return 101;
    }

    if ((a+b) < c || (b+c) < a || (a+c) < b || a < 1 || b < 1 || c < 1)
    {
        return 102;
    }

    printf("S = %0.2f\n", heron(a, b, c));

    return 0;
}
