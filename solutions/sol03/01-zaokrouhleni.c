/*
 * Napište program, který provede zaokrouhlení reálného 
 * čísla na požadovaný počet desetinných míst. 
 */

#include <stdio.h>
#include <math.h>

float zaokrouhleni (float c, int n)
{
    float x = pow(10, n);
    float p = 0.5;

    if (c < 0) p = -0.5;

    return ((int)(c*x + p))/x;
}

int main()
{
    float cislo;
    int pocet;

    scanf("%f%i", &cislo, &pocet);

    printf("%.*f", pocet, zaokrouhleni(cislo, pocet));

    return 0;
}
