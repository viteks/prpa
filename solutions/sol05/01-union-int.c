/*
 * sizeof:  char 1, short 2, int 4, ...
 * 
 * int:     | B0 | B1 | B2 | B3 |
 * char[4]: |    |    |    |    |
 */

#include <stdio.h>
#include <math.h>

union integer
{
    unsigned short a;
    unsigned char b[2];
};

// struktura? obsadila by 4B
// union? obsazuje tolik pameti, kolik ma nejvetsi prvek

int main()
{
    union integer a;

    for (int i = 250; i < 260; i++)
    {
        a.a = i;
        printf("%4i | %4i %4i\n", a.a, a.b[0], a.b[1]);
    }

    return 0;
}