#include <stdio.h>

void inicializace (int *a, int *b, int *c)
{
    *a = 20;
    *b = 30;
    *c = 40;
}

int main()
{
    int x = 0, y = 0, z = 0; 

    printf("pred volanim funkce: x = %d, y = %d, z = %d\n", x, y, z);

    inicializace(&x, &y, &z);

    printf("pred volanim funkce: x = %d, y = %d, z = %d\n", x, y, z);

    return 0;
}