#include <stdio.h>

int main(void)
{
	unsigned char A = 127;
	char B = 127;
	
	printf("Promenne A=%i a B=%i\n", A, B);
	A = A + 2;
	B = B + 2;
	printf("Promenne A=%i a B=%i\n", A, B);

	return 0;
}
