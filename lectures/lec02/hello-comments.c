/* vlozeni hlavickoveho souboru standardni knihovny */
#include <stdio.h>
// hlavicka funkce main()
int main() 
{	// hlavni funkce programu
	/* volani funkce printf() definovane ve knihovne stdio.h */
	printf("Uz programuju!\n");
	/* ukonceni funkce a predani navratove hodnoty 0 operacnimu systemu */
	return 0;
}