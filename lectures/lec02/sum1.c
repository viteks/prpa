#include <stdio.h>

int main(void)
{
	/* definice lokalni promenne typu int */
	int sum; 
	/* hodnota vyrazu se ulozi do sum */
	sum = 100 + 43; 
	printf("Soucet 100 a 43 je %i\n", sum);
	/* %i formatovaci prikaz pro tisk celeho cisla */
	return 0;
}