#include <stdio.h>

#define I   3
#define J   6

int main()
{
   int test = 3;

   for (int i = 0; i < I; ++i) {
      for (int j = 0; j < J; ++j) {
         if (j == test) {
            goto navesti;
         }
         fprintf(stdout, "Cyklus i: %d j: %d\n", i, j);
      }
   }
   return 0;
navesti:
   fprintf(stderr, "Po ukonceni cyklu.\n");
   return 100;
}