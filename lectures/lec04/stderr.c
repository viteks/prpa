#include <stdio.h>

int main(void)
{
	int ret = 0, a, b;

	fprintf(stdout, "Zadej jedno cele cislo: ");

	a = scanf("%d", &b);

	if (a > 1) 
	{
		fprintf(stdout, "Bylo zadano cislo %d\n", b);
	} 
	else 
	{
		fprintf(stdout, "Cislo nebylo zadano spravne.\n");
		fprintf(stderr, "I/O error\n");
		ret = -1;
	}
	return ret;
}