#include <stdio.h>
#include <stdint.h>

int main()
{
        uint8_t a = 10;

        for (int i = 7; i >= 0; --i)
        {
                printf ("%i", (a >> i) & 1);
        }

        printf("\n");

	return 0;
}
