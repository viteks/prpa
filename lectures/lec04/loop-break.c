#include <stdio.h>

int main(void)
{
   int i = 10, test = 5;

   while (i > 0) {
      if (i == test) {
         printf("podminka testu splnena, opoustim cyklus.\n");
         break;
      }
      i--;
      printf("Stav citace i: %d\n", i);
   }
   return 0;
}
