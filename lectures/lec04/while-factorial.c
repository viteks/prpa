#include <stdio.h>
#include <stdlib.h>

int main(void) {
	int i = 1, f = 1, n;
	printf("zadejte prirozene cislo: ");
	scanf("%d", &n);
	while (i<n) {
		i = i+1;
		f = f*i;
	}
	printf("%d! = %d\n", n, f);
	return 0;
}