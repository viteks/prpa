#include <stdio.h>

int main()
{
    int i = 0;
    int j = 100;

    do
    {
        printf("j = %i\n", j);
        j = j + 100;
        i = i + 1;
    } while (j < 5);

    return 0;
}