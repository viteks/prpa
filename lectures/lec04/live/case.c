#include <stdio.h>

int main()
{
    int n = 2;

    switch (n)
    {
        case 0:
            printf("n = %i\n", n);
        case 1:
            printf("n = %i\n", n);
            break;
        case 2:
            printf("n = %i\n", n);
        default:
            printf("nejaka jina hodnota\n");
    }

    return 0;
}