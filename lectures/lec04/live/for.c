#include <stdio.h>

int main()
{
    int i = 0;
    int j = 1;

    for (; i < 10; i=i+1)
    {
        printf("%i ", j+=2);
    }

    printf("\n");

    int a = 10;

    a++;

    printf("a = %i\n", a++);

    printf("a = %i\n", a);

    // v teto variante je hodnota i platna i zde a 
    // je rovna nejvyssi dosazene hodnote ridici promenne
}