#include <stdio.h>

int main(void) 
{
   
   int den;

   if (scanf ("%d", den) > 0) {
      switch (den) {
         case 1:
            printf("Pondeli\n");
            break;
         case 2:
            printf("Utery\n");
            break;
         case 3:
            printf("Streda\n");
            break;
         case 4:
            printf("Ctvrtek\n");
            break;
         case 5:
            printf("Patek\n");
            break;
         case 6:
            printf("Sobota\n");
            break;
         case 7:
            printf("Nedele\n");
            break;
         default:
            printf("Neplatny den v tydnu!\n");
      }
   }
   return 0;
}
