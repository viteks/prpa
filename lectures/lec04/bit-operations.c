#include <stdio.h>
#include <stdint.h>

const char * printByte (uint8_t a)
{
        static char ret[9] = {'\0'};

        for (int i = 7; i >= 0; --i)
        {
                ret[7-i] = (char)(((a >> i) & 1) + 48);
        }
        return ret;
}

int main()
{
        uint8_t a = 0x0D;
        uint8_t b = 0xAA;

        printf("--------\n");
        printf("%s\n", printByte(a));
        printf("       &\n");
        printf("%s\n", printByte(b));
        printf("       =\n");
        printf("%s\n", printByte(a&b));
        printf("--------\n");
        printf("%s\n", printByte(a));
        printf("       |\n");
        printf("%s\n", printByte(b));
        printf("       =\n");
        printf("%s\n", printByte(a|b));
        printf("--------\n");
        printf("%s\n", printByte(a));
        printf("       ^\n");
        printf("%s\n", printByte(b));
        printf("       =\n");
        printf("%s\n", printByte(a^b));
        printf("--------\n");
        return 0;
}
