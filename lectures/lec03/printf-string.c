#include <stdio.h>

#define STRING "Testovaci retezec!"

int main(void)
{
    printf("*%2s*\n", STRING);
    printf("*%24s*\n", STRING);
    printf("*%24.5s*\n", STRING);
    printf("*%-24.5s*\n", STRING);

    return 0;
}