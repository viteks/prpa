#include <stdio.h>  /* hlavickovy soubor */ 

#define PI 3.14     /* symbolicka konstanta - makro */

float kruh(int a);  /* hlavicka/prototyp funkce */

int main(void)      /* hlavni funkce */
{	
	int v = 10;       /* definice promennych */
	float r;
	r = kruh(v);      /* volani funkce */
	return 0;         /* ukonceni hlavni funkce */
}

float kruh(int r)   /* definice funkce */
{	
	float b = PI*r*r; 
	return b;        /* navratova hodnota funkce */
}