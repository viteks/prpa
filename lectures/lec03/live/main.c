#include <stdio.h>

#define PI  3.14
#define TRUE 1

const float pi = 3.1415;

// deklarace
void tisk ();

// definice funkce
// funkce ma navratovou hodnotu urciteho datoveho typu
void tisk () // procedura - nema navratovou hodnotu
{
    printf("ahoj ");
    return;
    printf("svete\n"); // tento radek se vubec nevykona
}

void tisk2(int a)
{
    printf("a = %i\n", a);
}

void soucet(int a, int b)
{
    // promenne a, b plati pouze v ramci tohoto bloku
    printf("%i + %i = %i", a, b, a+b);
}

int soucet2(int a, int b)
{
    int c; 
    c = a + b;
    return c;
}

int main()
{
    int x = 0xff;
    int y = 0b11111111;

    float z = 3.14;
    float u = 1.;

    int a = 1;  // 0x01 -> 1
    int b = '1'; // 0x31 -> 

    // b - 0x30 -> 1; b - '0'

    tisk2(1);
    tisk2(10);

    // soucet(10, 20);

    x = soucet2(10, 20);
    printf("soucet je %i\n", x);
    printf("soucet je %i\n", soucet2(30, 40));

    printf("pi = %.2f\n", PI);

    {
        
    }

    return 0;
}