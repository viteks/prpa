#include <stdio.h>

int main(void)
{
    int n;

    printf("Napis tri cela cisla:\n");
    scanf("%*d %*d %d", &n);
    printf("Posledni cislo je %d\n", n);

    return 0;
}