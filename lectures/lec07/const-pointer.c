#include <stdio.h>

int main(void)
{
	int v1 = 10, v2 = 20;
	int *const ptr = &v1;
	printf("v1: %d *ptr: %d\n", v1, *ptr);
	*ptr = 11; /* lze zmenit odkazovanou promennou */
	printf("v1: %d\n", v1);
#ifdef TEST
	// pro otestovani teto casti definujte makro TEST
	// nebo prelozte: cc -DTEST const-pointer.c
	ptr = &v2; /* NELZE! */
#endif
	return 0;     
}