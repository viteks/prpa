#include <stdlib.h>

int main(int argc, char *argv[])
{
	int *int_array;
	const int size = 4;

	allocate_memory(sizeof(int) * size, (void**)&int_array);
	fill_array(int_array, size);
	int *cur = int_array;
	
	for(int i = 0; i < size; ++i, cur++) {
		printf("Array[%d] = %d\n", i, *cur);
	}
	deallocate_memory((void**)&int_array);
	return 0;
}