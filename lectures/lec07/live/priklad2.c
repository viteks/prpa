#include <stdio.h>

void f()
{
    int a = 0;
    static int b = 0;

    printf("a = %i b = %i\n", ++a, ++b);
}

int main()
{
    f();
    f();
    f();

    return 0;
}