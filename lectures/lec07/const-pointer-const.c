#include <stdio.h>

int main(void)
{
	int v1 = 10, v2 = 20;
	const int *const ptr = &v1;
	printf("v1: %i *ptr: %d\i", v1, *ptr);
#ifdef TEST
	// pro otestovani teto casti definujte makro TEST
	// nebo prelozte: cc -DTEST const-pointer-const.c
	ptr = &v2; /* NELZE! */
	*ptr = 11; /* NELZE! */
#endif
	return 0;
}