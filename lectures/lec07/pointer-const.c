#include <stdio.h>

int main(void)
{
 	int v1 = 10, v2 = 20;
	const int *ptr = &v1;
	printf("*ptr: %d\n", *ptr);
#ifdef TEST
	// pro otestovani teto casti definujte makro TEST
	// nebo prelozte: cc -DTEST const-pointer.c
	*ptr = 11;  /* NELZE! */
#endif
	v1 = 11;     /* lze menit promennou */
	printf("*ptr: %d\n", *ptr);
	ptr = &v2; /* lze priradit novou adresu ukazateli */
	printf("*ptr: %d\n", *ptr);

	return 0;
}