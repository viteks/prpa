#include <stdio.h>

int main(int argc, char *argv[])
{
	printf("Pocet argumentu %i\n", argc);
	for (int i = 0; i < argc; ++i) {
		printf("argv[%i] = %s\n", i, argv[i]);
	}
	return argc > 0 ? 0 : 1;
}