#include <stdio.h>

void fce(int A[]) {
	int B[] = { 2, 4, 6 };
	printf("[A] = %lu, [B] = %lu\n", sizeof(B), sizeof(B));
	for (int i = 0; i < 3; ++i) {
		printf("A[%i]=%i B[%i]=%i\n", i, A[i], i, B[i]);
	}
}

int main(void)
{
	int array[] = { 1, 2, 3 };
	fce(array);
	return 0;
}