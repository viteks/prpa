#include <stdio.h>

int main(void)
{
	int m[3][3] = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
	printf("[m]: %lu == %lu\n", sizeof(m), 3*3*sizeof(int));

	for (int r = 0; r < 3; ++r) {
		for (int c = 0; c < 3; ++c) {
			printf("%3i", m[r][c]);
		}
		printf("\n");
	}

	return 0;
}