#include<stdio.h>
 
int main(void)
{
    int *p;        // ukazatel na integer     
    int (*ptr)[5]; // ukazatel na pole peti prvku int 
    int arr[5];
    
    p = arr;       // ukazatel na prvni prvek pole
    ptr = &arr;    // ukazatel na cele pole 

    printf("sizeof(p) = %lu\n", sizeof(p));
    printf("sizeof(ptr) = %lu\n", sizeof(ptr));
    printf("p = %p, ptr = %p\n", p++, ptr++);     
    printf("p = %p, ptr = %p\n", p, ptr);
     
    return 0;
}