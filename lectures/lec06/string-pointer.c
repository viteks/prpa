#include <stdio.h>

int main()
{
	char a[] = "ahoj";
	char *b = "ahoj";
	char *c = "ahoj";

	printf ("adresa a: %p\n", a);
	printf ("adresa b: %p\n", b);
	printf ("adresa c: %p\n", c);

	a[0] = 'A';
	printf ("a = %s\n", a);
	// b[0] = 'A'; NELZE -- RO segment pameti

	return 0;
}
