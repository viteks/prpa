#include <stdio.h>

int main(int argc, char *argv[])
{
	int ret = 0;

	fprintf(stdout, "Jmeno programu je %s\n", argv[0]);

	if (argc > 1) 
	{
		fprintf(stdout, "Prvni argument je %s\n", argv[1]);
	} 
	else 
	{
		fprintf(stdout, "Prvni argument nebyl zadan.\n");
		fprintf(stderr, "Musi byt zadan alespon jeden argument!\n");
		ret = -1;
	}
	return ret;
}