#include <stdio.h>
#include <stdlib.h>

#define MAX 100

int ctiInt(int min, int max) {
/* precte cislo, zkontroluje, zda je v intervalu min az max
   a kdyz ne, cteni se opakuje
*/
  int x;
  scanf("%d", &x);
  while (x<min || x>max) {
    printf("spatne, zadejte znovu: ");
    scanf("%d",&x);
  }
  return x;
}

void ctiVektor(int v[], int n) {
  int i;
  printf("zadejte %d celych cisel\n", n);
  for (i=0; i<n; i++)
    scanf("%d", &v[i]);
}

int skalarniSoucin(int x[], int y[], int n) {
  int i, s=0;
  for (i=0; i<n; i++)
    s += x[i]*y[i];
  return s;
}

int main(void) {
  int x[MAX], y[MAX], n;
  printf("zadejte pocet slozek vektoru (cislo od 1 do %d): ", MAX);
  n = ctiInt(1, MAX);
  printf("vektor x\n");
  ctiVektor(x, n);
  printf("vektor y\n");
  ctiVektor(y, n);
  printf("skalarni soucin vektoru x a y je %d\n", skalarniSoucin(x, y, n));
  system("PAUSE");
  return 0;
}
