#include <stdio.h>

int main(void)
{
	char str[] = "123";
	char s[] = {'5', '6', '7'};

	printf("sizeof(str): %lu\n", sizeof(str));
	printf("sizeof(s)  : %lu\n", sizeof(s));
	printf("str: '%s'\n", str);
	printf("  s: '%s'\n", s);

	return 0;
}