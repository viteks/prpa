#include <stdio.h>

int main(void)
{
	char a[] = "hello";
	a[0] = 'H';

	char *b = "hello";
	b[0] = 'H';

	printf("sizeof (a): %lu\n", sizeof(a));

	return 0;
}
