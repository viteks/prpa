#include <stdio.h>

void tisk (int *x, int velikost)
{
    for (int i = 0; i < velikost; i++)
    {
        printf("%i ", *(x++)); // *(x+i) vs. *(x++)
    }
    printf("\n");
}


int main()
{
    int a[] = {10, 20, 30, 40, 50};

    tisk(a, sizeof(a)/sizeof(int));
}