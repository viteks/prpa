#include <stdio.h>

int main()
{

    int a[][3] = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
    // int a[3][3] = {1, 2, 3, 4, 5, 6, 7, 8, 9};

    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            printf("%i ", a[i][j]);
        }
        printf("\n");
    }
    printf("\n");

    int *p = a[0];
    /*
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            printf("%i ", *(p++));
        }
        printf("\n");
    }
    printf("\n");
    */
    int (*q)[3] = a;

    printf("p: %p %i\n", a, *(p));
    // printf("q: %p %i\n", q, *((int *)(q+1)));
    printf("q: %p %i\n", q, *((int *)(q+1)));

    for (int i = 0; i < 3; i++)
    {
        printf("%i ", *((int *)q++));
    }
    printf("\n");
}