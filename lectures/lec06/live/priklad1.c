#include <stdio.h>

int main()
{
    // pole, nekde v pameti
    int b;  // -> bezna hodnota
    int a[] = {10, 20, 30};
    
    printf("adresa b: %p\n", &b);
    printf("adresa a: %p\n", a);

    int *p = a;

    printf("obsah p: %p\n", p);

    printf("p[1] = %i\n", p[1]);

    printf("p[1] = %i\n", *(p+1)); // adresa uchovana v p + 1*sizeof(int)

    for (int i = 0; i < 3; i++)
    {
        printf("%i ", *(p+i)); // p[0] ~ *(p+0); p[1] ~ *(p+1); 
    }

    printf("\n");

    int (*c)[3] = &a;   // ukazatel na cele triprvkove pole typu int

    printf("obsah p: %p\n", p);
    printf("obsah c: %p\n", c);

    printf("adresa p+1: %p\n", p+1);
    printf("adresa c+1: %p\n", c+1);

    return 0;
}