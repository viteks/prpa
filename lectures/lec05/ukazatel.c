#include <stdio.h>

int main()
{
    int a = 10; // prime adresovani
    /*
        scanf ("%i", &a);
    */

    // adresa promemne v pameti -> &a
    // promenna typu ukazatel na datovy typ int, ktera ma vyznam adresy(!) 
    // je inicializovana adresou prommene a
    int * b = &a;   // ziskavam referenci na promennou

    printf("a = %i\n", a);

    printf("adresa prommne a: %p\n", &a);
    printf("adresa prommne a: %p\n", b);
    
    // dereference
    // nastav obsah pameti na adrese, ktera je ulozena v promemne b
    *(b) = 22;

    printf("a = %i\n", a);

}