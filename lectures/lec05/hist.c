#include <stdio.h>

int main(void)
{
	int h[26] = {0};
	char a;

	while(scanf("%c", &a) != EOF) {
		if('A' <= a && a <= 'Z') h[a-'A']++;
	}

	for (int i = 0; i < 26; i++) {
		printf("%c:% 4i\n", i+'A', h[i]);
	}

	return 0;
}