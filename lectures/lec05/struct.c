#include <stdio.h>

struct clovek 
{
    unsigned char vaha;  // 0 - 255
    unsigned char vyska; // 
    double vek;
} ;

typedef struct 
{
    unsigned char vaha;  // 0 - 255
    unsigned char vyska; // 
    double vek;
} CLOVEK;

int main()
{
    struct clovek josef;
    CLOVEK martin;

    josef.vaha = 70; // ~ int josef_vaha
    josef.vyska = 175; // ~ int josef_vyska
    josef.vek = 19.9;

    printf("%i %i %f", josef.vaha, josef.vyska, josef.vek);

    martin.vaha = 80;
    martin.vyska = 190;
    martin.vek = 20.0;
}
