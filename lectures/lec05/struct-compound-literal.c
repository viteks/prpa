#include <stdio.h>

struct point {
        int x, y;
};

int main(void)
{
        struct point b = { .y = 3, .x = 2 };

        // b = {5, 6};                  NELZE!
        // b = { .x = 5, .y = 6 };      NELZE!
        b.x = 5;
        b.y = 6;        // C89
        // v C99 byl zaveden compound literal,
        // ktery vytvori anonymni strukturu, kterou
        // lze inicializovat a priradit
        b = (struct point){5, 6};
        b = (struct point){ .x = 5, .y = 6 };

        return 0;
}

/*

union n
{
        unsigned char x; 
        unsigned int  y;
}

| 0 | 1 | 2 | 3 |

little endian

n.x = 10;
n.y? -> 10

n.y = 256;
n.x ? -> 1
*/

