#include<stdio.h>
 
int main()
{
    int *p;		// ukazatel na integer     
    int (*ptr)[5];	// ukazatel na pole peti prvku int. 
    int arr[5];
    
    p = arr;		// ukazatel na prvni prvek pole
    ptr = &arr;		// ukazatel na cele pole 

    printf("p = %p, ptr = %p\n", p, ptr);
     
    p++; 
    ptr++;
     
    printf("p = %p, ptr = %p\n", p, ptr);
     
    return 0;
}