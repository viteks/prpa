#include <stdio.h>
#include <stdlib.h>

int tabulka[100];

void ctiCisla(void) {
  int cislo;
  printf("zadejte posloupnost celych cisel" " zakoncenou nulou\n");
  scanf("%d", &cislo);
  while (cislo!=0) {
    if (cislo>=1 && cislo<=100)
      tabulka[cislo-1]++;
    scanf("%d", &cislo);
  }
}

void vypisTabulku(void) {
  int i;
  for (i=1; i<=100; i++)
    if (tabulka[i-1])
      printf("pocet vyskytu cisla %2d je %d\n", i, tabulka[i-1]);
  printf("\n");
}

int main(void) {
  printf("tabulka cetnosti cisel z intervalu 1 az 100\n");
  ctiCisla();
  vypisTabulku();
  system("PAUSE");
  return 0;
}
