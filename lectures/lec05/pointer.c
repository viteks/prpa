#include <stdio.h>

int main()
{
	int a = 10;
	int *p = &a;
	printf("a = %i\n", a);
	*p = 20;
	printf("a = %i\n", a);

	return 0;
}