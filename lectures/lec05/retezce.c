/* prog7-retezce1.c */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXDELKA 100

int main(void) {
  char slovo1[MAXDELKA], slovo2[MAXDELKA], vetsi[MAXDELKA];
  printf("zadejte prvni slovo: ");
  scanf("%s", slovo1);
  printf("zadejte druhe slovo: ");
  scanf("%s", slovo2);
  printf("slovo %s ma %d znaku\n", slovo1, strlen(slovo1));
  printf("slovo %s ma %d znaku\n", slovo2, strlen(slovo2));
  if (strcmp(slovo1, slovo2)>0)
    strcpy(vetsi, slovo1);
  else
    strcpy(vetsi, slovo2);
  printf("vetsi je %s\n", vetsi);
  system("PAUSE");
  return 0;
}
