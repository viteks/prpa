#include <stdio.h>

int main()
{
	int teploty[7], prumer = 0;

	for (int i = 0; i < 7; i++)
	{
		scanf("%d", &teploty[i]);
		prumer += teploty[i];
	}
	printf("%d\n", prumer);
	return 0;
}