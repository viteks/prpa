#include <stdio.h>

int main(void) 
{
	// int array[5];
	// int array[] = {2, 3, 4, 5, 6};
	int array[] = {[4]=20};

	for (int i = 0; i < sizeof(array)/sizeof(int); i++)
	{
		array[i] = i + 10;
	}

	printf("Size of array: %lu\n", sizeof(array));

	for (int i = 0; i < 5; ++i) {
		printf("Item[%i] = %i\n", i, array[i]);
	}

	return 0;
}